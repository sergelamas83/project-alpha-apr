from django.urls import path
from . import views

urlpatterns = [
    path("", views.list_projects, name="home"),
    path("list_projects/", views.list_projects, name="list_projects"),
    path("<int:id>/", views.show_project, name="show_project"),
    path("projects/create/", views.create_project, name="create_project"),
]
